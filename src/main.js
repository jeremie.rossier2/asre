import { createApp } from 'vue'
import App from './App.vue'
import Notifications from 'notiwind'
import router from './router'
import { createPinia } from 'pinia'

import './style.css'
import 'remixicon/fonts/remixicon.css';

const pinia = createPinia()
const app = createApp(App)

app.use(pinia)
app.use(router)
app.use(Notifications)

app.mount('#app')