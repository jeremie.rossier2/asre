const CategoriesInterestEnum = Object.freeze([
    {
        id: 'cottage',
        value: 'Gîte',
        icon: 'ri-home-heart-fill',
        background: 'bg-rose-600',
        classes: 'ring-rose-600'
    },
    {
        id: 'restaurant',
        value: 'Restaurant',
        icon: 'ri-restaurant-2-line',
        background: 'bg-lime-700',
        classes: 'ring-lime-700'

    },
    {
        id: 'parking',
        value: 'Parking',
        icon: 'ri-parking-box-line',
        background: 'bg-blue-600',
        classes: 'ring-blue-600'

    },
    {
        id: 'water',
        value: 'Point d\'eau',
        icon: 'ri-drop-fill',
        background: 'bg-sky-400',
        classes: 'ring-sky-400'
    },
    {
        id: 'accomodation',
        value: 'Autre hébergement',
        icon: 'ri-hotel-bed-fill',
        background: 'bg-neutral-500',
        classes: 'ring-neutral-500'
    },
]);

export default CategoriesInterestEnum;