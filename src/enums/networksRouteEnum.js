const NetworksRouteEnum = Object.freeze([
    {
        id: 'arec',
        value: 'AREC',
    },
    {
        id: 'arej',
        value: 'AREJ',
    },
    {
        id: 'aref',
        value: 'AREF',
    },
    {
        id: 'aren',
        value: 'AREN',
    },
    {
        id: 'avic',
        value: 'AVIC',
    },
    {
        id: 'equivia',
        value: 'Equivia',
    },
    {
        id: 'zkv',
        value: 'ZKV'
    }

]);

export default NetworksRouteEnum;