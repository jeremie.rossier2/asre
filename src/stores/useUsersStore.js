import {defineStore} from "pinia";
import {supabase} from "../supabase.js";

export const useUsersStore = defineStore('users', {
    state: () => ({profile: null, profiles: [], session: null}),
    getters: {
        getSession: (state) => state.session,
        getProfile: (state) => state.profile,
        getProfiles: (state) => state.profiles,
    },
    actions: {
        setSession(session) {
            this.session = session;
        },
        setProfile(profile) {
            this.profile = profile
        },
        setProfiles(profiles) {
            this.profiles = profiles;
        },

        async updateAvatar(file) {
            const extension = file.name.split('.').pop();
            const {data, error} = await supabase
                .storage
                .from('avatars')
                .upload(`public/avatar-${this.profile?.id}.${extension}`, file, {
                    cacheControl: '3600',
                    upsert: true,
                })
            return error ? error : data;
        },

        async updateUser(user) {
            const {data, error} = await supabase.auth.updateUser({
                email: user.email,
                password: user.password
            });
            return error ? error : data;
        },

        async updateCurrentProfile(profile) {
            let {data, error} = await supabase.from('profiles').update({
                full_name: profile.full_name,
                role: profile.role,
                avatar_url: profile.avatar_url,
            }).eq('id', this.profile?.id).select()
            if (error) {
                return error;
            } else {
                this.setProfile(data[0])
                return data[0];
            }
        },

        async getProfileFromDB(id) {
            const {data, error} = await supabase
                .from('profiles')
                .select()
                .eq('id', id);
            if (data) {
                this.setProfile(data[0])
            }
            return error;
        },

        async getProfilesFromDB() {
            const {data, error} = await supabase
                .from('profiles')
                .select()
            if (data) {
                this.setProfiles(data)
            }
            return error;
        }
    },
})