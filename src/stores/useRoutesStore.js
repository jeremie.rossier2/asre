import {defineStore} from "pinia";
import {supabase} from "../supabase.js";

export const useRoutesStore = defineStore('routes', {
    state: () => ({ routes: [], route: null}),
    getters: {
        getRoutes: (state) => state.routes,
        getRoute: (state) => state.route,
    },
    actions: {
        setRoutes(routes) {
            this.routes = routes;
        },
        setRoute(route) {
            this.route = route;
        },

        async getRoutesFromDB() {
            const { data, error } = await supabase
                .from('routes')
                .select('*, waypoints(*)')
                .order('order', { foreignTable: 'waypoints', ascending: true });
            if (data) {
                this.setRoutes(data)
            }
            return error;
        },

        async uploadRouteImage(file, route) {
            const extension = file.name.split('.').pop();
            const { data, error } = await supabase
                .storage
                .from('routes')
                .upload(`${route.id}/image.${extension}`, file, {
                    cacheControl: '3600',
                    upsert: true,
                });

            if (error) {
                return error;
            } else {
                route.image_url = import.meta.env.VITE_SUPABASE_ROUTES_STORAGE_URL + data.path;
                return await this.updateRouteImage(route)
            }
        },

        async updateRouteImage(route) {
            let { data, error } = await supabase
                .from('routes')
                .update({
                    image_url: route.image_url,
                })
                .eq('id', route.id)
                .select();
            if (!error) {
                error = await this.getRoutesFromDB();
            }

            return error ? error : data;
        },

        async removeRouteImage(route) {
            if (route.image_url) {
                const extension = route.image_url.split('.').pop();

                const { data, error } = await supabase
                    .storage
                    .from('routes')
                    .remove([`${route.id}/image.${extension}`]);
            }
        },

        async createRoute(route, waypoints, image_file) {
            let { data, error } = await supabase
                .from('routes')
                .insert({
                    name: route.name,
                    short_desc: route.short_desc,
                    long_desc: route.long_desc,
                    network: route.network,
                    difficulty: route.difficulty,
                    geojson: route.geojson
                })
                .select();

            if (!error && waypoints?.length > 1) {
                waypoints.forEach(waypoint => {
                    waypoint.route_id = data[0].id;
                });
                error = await this.insertWaypoints(waypoints);
            }
            if (!error && image_file) {
                error = await this.uploadRouteImage(image_file, data[0])
            }

            if (!error) {
                error = await this.getRoutesFromDB();
            }

            return error ? error : data;
        },

        async updateRoute(route, waypoints, image_file) {
            let { data, error } = await supabase
                .from('routes')
                .update({
                    name: route.name,
                    short_desc: route.short_desc,
                    long_desc: route.long_desc,
                    network: route.network,
                    difficulty: route.difficulty,
                    geojson: route.geojson,
                })
                .eq('id', route.id)
                .select();

            if (!error && waypoints?.length > 1) {
                let waypointsToUpdate = [];
                let waypointsToInsert = [];
                let oldWaypoints = await this.getWaypointsOfRouteFromDB(route.id);

                let waypointsToDelete = oldWaypoints.filter(function(old) {
                    return !waypoints.some(function (waypoint) {
                       return old.id === waypoint.id;
                    });
                });

                waypoints.forEach(waypoint => {
                    waypoint.route_id = data[0].id;
                    if (waypoint?.id) {
                        waypointsToUpdate.push(waypoint);
                    } else {
                        waypointsToInsert.push(waypoint)
                    }
                });

                error = await this.insertWaypoints(waypointsToInsert);
                error = await this.upsertWaypoints(waypointsToUpdate);
                for (const waypoint of waypointsToDelete) {
                    error = await this.deleteWaypoint(waypoint);
                }
            }

            if (!error && image_file) {
                if (data[0].image_url) {
                    await this.removeRouteImage(data[0]);
                }
                error = await this.uploadRouteImage(image_file, data[0])
            }

            if (!error) {
                error = await this.getRoutesFromDB();
            }

            return error ? error : data;
        },

        async deleteRoute(route) {
            const { error } = await supabase
                .from('routes')
                .delete()
                .eq('id', route.id);

            if (!error) {
                await this.removeRouteImage(route);
                return await this.getRoutesFromDB();
            }

            return error;
        },

        async getRouteFromDB(id) {
            const { data, error } = await supabase
                .from('routes')
                .select('*, waypoints(*)')
                .order('order', { foreignTable: 'waypoints', ascending: true })
                .eq('id', id);
            if (data) {
                this.setRoute(data[0])
            }
            return error ? error : data;
        },

        async upsertWaypoints(waypoints) {
            const { error } = await supabase
                .from('waypoints')
                .upsert(waypoints);
            return error;
        },

        async insertWaypoints(waypoints) {
            const { error } = await supabase
                .from('waypoints')
                .insert(waypoints);
            return error;
        },

        async deleteWaypoint(waypoint) {
            const { error } = await supabase
                .from('waypoints')
                .delete()
                .eq('id', waypoint.id);
            return error;
        },

        async getWaypointsOfRouteFromDB(routeId) {
            const { data, error } = await supabase
                .from('waypoints')
                .select()
                .eq('route_id', routeId)
                .order('order', { ascending: true });

            return error ? error : data;
        },
    },
})