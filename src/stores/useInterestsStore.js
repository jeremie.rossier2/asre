import {defineStore} from "pinia";
import {supabase} from "../supabase.js";

export const useInterestsStore = defineStore('interests', {
    state: () => ({ interests: [] }),
    getters: {
        getInterests: (state) => state.interests,
    },
    actions: {
        setInterests(interests) {
            this.interests = interests;
        },

        async getInterestsFromDB() {
            const { data, error } = await supabase.from('interests').select();
            if (data) {
                this.setInterests(data)
            }
            return error;
        },

        async uploadInterestImage(file, interest) {

            const extension = file.name.split('.').pop();
            const { data, error } = await supabase
                .storage
                .from('interests')
                .upload(`${interest.id}/image.${extension}`, file, {
                    cacheControl: '3600',
                    upsert: true,
                });

            if (error) {
                return error;
            } else {
                interest.image_url = import.meta.env.VITE_SUPABASE_INTERESTS_STORAGE_URL + data.path;
                return await this.updateInterest(interest, null)
            }
        },

        async removeInterestImage(interest) {
            if (interest.image_url) {
                const extension = interest.image_url.split('.').pop();

                const { data, error } = await supabase
                    .storage
                    .from('interests')
                    .remove([`${interest.id}/image.${extension}`]);
            }
        },

        async createInterest(interest, image_file) {
            let { data, error } = await supabase
                .from('interests')
                .insert(interest)
                .select();

            if (!error && image_file) {
                error = await this.uploadInterestImage(image_file, data[0])
            }
            if (!error) {
                error = await this.getInterestsFromDB();
            }

            return error ? error : data;
        },

        async updateInterest(interest, image_file) {
            let { data, error } = await supabase
                .from('interests')
                .update(interest)
                .eq('id', interest.id)
                .select();

            if (!error && image_file) {
                if (data[0].image_url) {
                    await this.removeInterestImage(data[0]);
                }
                error = await this.uploadInterestImage(image_file, data[0])
            }

            if (!error) {
                error = await this.getInterestsFromDB();
            }

            return error ? error : data;
        },

        async deleteInterest(interest) {
            const { error } = await supabase
                .from('interests')
                .delete()
                .eq('id', interest.id);

            if (!error) {
                await this.removeInterestImage(interest);
                return await this.getInterestsFromDB();
            }

            return error;
        },

        async getInterestFromDB(id) {
            const { data, error } = await supabase
                .from('interests')
                .select()
                .eq('id', id);
            return error ? error : data;
        },

    },
})