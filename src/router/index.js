import {createWebHistory, createRouter} from "vue-router";
import InteractiveMap from "../pages/interactiveMap/InteractiveMap.vue";
import Users from "../pages/users/Users.vue";
import Interests from "../pages/interests/Interests.vue";
import Routes from "../pages/routes/Routes.vue";
import Login from "../pages/Login.vue";
import {useUsersStore} from "../stores/useUsersStore.js";
import Profile from "../pages/Profile.vue";
import EditInterest from "../pages/interests/EditInterest.vue";
import EditRoute from "../pages/routes/EditRoute.vue";
import ShowInterest from "../pages/interests/ShowInterest.vue";
import ShowRoute from "../pages/routes/ShowRoute.vue";

const routes = [
    {
        path: "/",
        name: "Carte intéractive",
        component: InteractiveMap,
    },
    {
        path: "/login",
        name: "Se connecter",
        component: Login,
    },
    {
        path: "/routes",
        name: "Parcours",
        component: Routes,
    },
    {
        path: "/routes/edit/:id?",
        name: "Créer un parcours",
        component: EditRoute,
        props: true,
    },
    {
        path: "/routes/show/:id?",
        name: "Consulter un parcours",
        component: ShowRoute,
        props: true,
    },
    {
        path: "/interests",
        name: "Points d'intérêts",
        component: Interests,
    },
    {
        path: "/interests/edit/:id?",
        name: "Créer un point d'interêt",
        component: EditInterest,
        props: true,
    },
    {
        path: "/interests/show/:id?",
        name: "Consulter un point d'interêt",
        component: ShowInterest,
        props: true,
    },
    {
        path: "/users",
        name: "Utilisateurs",
        component: Users,
    },
    {
        path: "/profile",
        name: "Profil",
        component: Profile,
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});


router.beforeEach(async (to, from) => {
    const usersStore = useUsersStore()

    if (to.path !== '/' && !to.path.includes('show')) {
        if (!usersStore.session && to.path !== '/login') {
            return {path: '/login'}
        }
    }

    if (usersStore.session && (to.path === '/login')) {
        return {path: '/'}
    }
})

export default router;